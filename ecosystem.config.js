module.exports = {
  apps : [{
        name: process.env.DB_NAME,
        script: 'lib/server.js',
        env_production: {
         NODE_ENV: 'production',
		 SECRET: process.env.SECRET
    }
  }],
  // Deployment Configuration
  deploy : {
    production : {
       user: process.env.SSH_USER,
       host: process.env.SSH_HOST,
       ref: `origin/${process.env.DEPLOY_BRANCH}`,
	   repo: process.env.REPOSITORY_URL,
       path: process.env.DEPLOY_DST,
       ssh_options: ['StrictHostKeyChecking=no'],
	   "post-deploy" : "cp ../.env . && npm install && pm2 startOrRestart ecosystem.config.js --env production"
    }
  }
};